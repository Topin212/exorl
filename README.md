# README #
## Exorl ##
* This is a rogue-like project for exam
* Current version 0.9.1 ([download](https://dropmefiles.com/FQOzQ))

## You will need ##
* Windows xp or higher
* mingw compiler(latest recommended)

## How do I get set up? ##
* compile the project(codeLight recommended, VS not checked)
* *not checked* try naming your .exe "Exorl.exe"
* create some custom levels, if you want and put them in a folder next to .exe file
* start Exorl file, enter the name of a folder, that contains levels
* enjoy, this should work

## Contribution guidelines ##

* Email me, if you want to contribute into the code
* You can contribute your own custom levels, but there's no place to hold them yet

## Who do I talk to? ##
If you have suggestions/questions/etc, feel free to email me, and tag the letter as Exorl