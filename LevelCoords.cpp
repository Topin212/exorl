#include "LevelCoords.h"

LevelCoords& LevelCoords::invertCoords(){
    int temp = x;
    x = y;
    y = temp;
    return *this;
}

LevelCoords::operator bool() const{
    if(x && y){
        return true;
    }
    return false;
}
int LevelCoords::operator==(LevelCoords lc){
    if(x == lc.getX() && y == lc.getY())
        return 1;
    else
        return 0;
}

LevelCoords::LevelCoords(int X, int Y)
    : x(X), y(Y){
}

LevelCoords::LevelCoords(): x(0), y(0){}


int LevelCoords::getX(){
   return x; 
}

int LevelCoords::getY(){
    return y;
}

void LevelCoords::setX(int X){
    x = X;
}

void LevelCoords::setY(int Y){
    y=Y;
}

void LevelCoords::set(int X, int Y){
    x = X;
    y = Y;
}

void LevelCoords::set(LevelCoords levelCoords){
    x = levelCoords.getX();
    y = levelCoords.getY();
}

void LevelCoords::set(LevelCoords *levelCoords){
    x = levelCoords->getX();
    y = levelCoords->getY();
}