#ifndef UTIL_H
#define UTIL_H

//this helps to set consoleCursorPosition
#include <windows.h>
//for printing out chars
#include <iostream>
//we need something to set the cursor, right?
#include "LevelCoords.h"
//for reading the level
#include <fstream>
//for storing the level
#include <vector>
#include <array>
#include <list>
#include <string>
//This is temporary, to use system("pause") and rand(). When a better way will be found, we'll get rid of this
#include <stdlib.h>
#include <time.h>
//for calculating distance from enemy to player
#include <math.h>
//for transforming strings to lower/uppercase
#include <algorithm>
//for taking input
#include <conio.h>


using std::cout;
using std::endl;
using std::string;
using std::fstream;
using std::vector;
using std::ios;
using std::array;
using std::to_string;
using std::transform;
using std::list;

class Util
{
public:
    Util();
    
    static HANDLE               consoleHandle;
    
    static LevelCoords          output;
    static LevelCoords          outputEnemy;
    static LevelCoords          outputExtra;
    
    static void                 setConsoleCursorPosition(LevelCoords);
    static void                 drawConsoleCursorPosition(LevelCoords, char);
    static void                 drawConsoleCursorPosition(LevelCoords, char, int);
    
    static string               generateName();
    
    static void                 eraseTile(LevelCoords);
    
    static string               toLowerCase(string, char);
    
    static char                 getInput();

    static void                 outputInfoColor(string,int);
    static void                 outputInfoColor(string, LevelCoords, int);
    
    static void                 outputInfo(string, LevelCoords=LevelCoords());
    static void                 outputInfo(int, LevelCoords=LevelCoords());
    static void                 outputInfo(LevelCoords, LevelCoords=LevelCoords());
    
    static int                  generateRandomNumber(int, int);
    ~Util();

};
#endif