#ifndef GAMEMANAGER_H
#define GAMEMANAGER_H

#include "Player.h"
#include "Enemy.h"
#include "LevelCoords.h"
#include "Level.h"
//#include "Util.h"


using namespace std;

class GameManager
{
public:
    string                      levelFullFileName;
    string                      levelsFolder;
    
    string                      generateLevelFileName(string);
    string                      generateLevelFileNameAtIndex(string, int);
    int                         levelIndex;
    
    void                        processInput(char);
    void                        movePlayer(char, char);
    void                        fightEnemy(LevelCoords);
    
    void                        moveEnemy(Enemy*, char);
    
    int                         redrawEnemy(Enemy*);
    void                        redrawPlayer();
    
    void                        gameLoop();
    void                        getInput();
    
    GameManager(string levelsFolder);
    ~GameManager();
private:
    Level                       level;
    Player                      player;
};

#endif // GAMEMANAGER_H
