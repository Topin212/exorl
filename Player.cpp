#include "Player.h"

void Player::die(){
    string output = name + " was killed!";
    Util::outputInfoColor(output, LevelCoords(5, 23), 2);
}

int Player::getXp(){
    return xp;
}

void Player::setXp(int _xp){
    xp = _xp;
}

void Player::rest(){
    hp++;
}

void Player::takeDamage(int _damage){
    if(_damage>ap)
        _damage -= ap;
    else
        _damage = 0;
    hp -= _damage;
    if(hp<0){
            string output = name + " was killed!";
    Util::outputInfoColor(output, LevelCoords(5, 12), 2);
        die();
    }
}

int Player::dealDamage(){
    return this->damage;
}

Player::Player(string _name, int _hp, int _ap, int _damage, int _xp, LevelCoords _coords): hp(_hp), ap(_ap), damage(_damage), xp(_xp){
    playerSymbol = '@';
    if(_name.empty()){
        _name = Util::generateName();
    }
    prevUnitCoords = unitCoords = _coords;
}

Player::Player()
{
    unitCoords.set(2,2);
    prevUnitCoords.set(unitCoords);
    
    hp = 10;
    ap = 0;
    damage = 20;
    
    unitSymbol = '@';
    unitName = "player";
}

void Player::playerInit(int _hp, int _ap, int _dmg){
    hp = _hp;
    ap = _ap;
    damage = _dmg;
}

Player::~Player()
{
}

int Player::getHp(){
    return hp;
}