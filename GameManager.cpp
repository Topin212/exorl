#include "GameManager.h"
string GameManager::generateLevelFileName(string _levelsFolder){
    levelFullFileName = _levelsFolder + _levelsFolder + _levelsFolder + ".lvl";
    //return string(_levelsFolder+string("\\level")+to_string(levelIndex) + string(".lvl"));
    return levelFullFileName;
}

string GameManager::generateLevelFileNameAtIndex(string _levelsFolder, int _levelIndex){
    return string(_levelsFolder+string("\\level")+to_string(_levelIndex) + string(".lvl"));
}

void GameManager::processInput(char _dir){
    switch(level.getTile(player.unitCoords, _dir)){
        case '#':
            //Add a rest function here
            player.rest();
            break;
        case '.':
            //move the player;
            movePlayer(_dir, level.getTile(player.unitCoords, _dir));
            break;
        case '>':
            //Enter next level
            movePlayer(_dir, level.getTile(player.unitCoords, _dir));
            break;
        case '<':
            //come back a level
            movePlayer(_dir, level.getTile(player.unitCoords, _dir));
            break;
        case '|':
            //this is a door, open it if you have a key
            break;
        default :
            //fight a monster?
            fightEnemy(level.getAdjTile(player.unitCoords, _dir).invertCoords());
            break;
    }
    
}

void GameManager::movePlayer(char _dir, char replace){
    Util::drawConsoleCursorPosition(player.unitCoords, player.unitSymbol);
    level.setTile(player.unitCoords, player.unitSymbol);
    
    Util::drawConsoleCursorPosition(player.prevUnitCoords, replace);
    level.setTile(player.prevUnitCoords, replace);
    
    
    switch(_dir){
        case 'r':
                player.unitCoords.setX(player.unitCoords.getX()+1);
            break;
        case 'l':  
                player.unitCoords.setX(player.unitCoords.getX()-1);
            break;
        case 'u':
                player.unitCoords.setY(player.unitCoords.getY()-1);
            break;
        case 'd':   
                player.unitCoords.setY(player.unitCoords.getY()+1);
            break;
    }
    Util::drawConsoleCursorPosition(player.unitCoords, player.unitSymbol);
    level.setTile(player.unitCoords, player.unitSymbol);
    
    player.prevUnitCoords = player.unitCoords;
    
}

void GameManager::fightEnemy(LevelCoords _target){
    //Util::outputInfo(_target);
    try{
        string outputStr = "You hit " + level.getEnemy(_target).type + " with your bare hands!";
        Util::outputInfo(outputStr, LevelCoords(5,19));
        level.getEnemy(_target).takeDamage(player.dealDamage()); //this guy throws exc

        if(level.getEnemy(_target).getHp() < 0){
            player.setXp(player.getXp() + level.getEnemy(_target).getXpValue());
        }
        //Util::outputInfo(tempEnemy.hp);
    }catch(unsigned int e){
        string outputString("Element #" + to_string(e) + " not found");
        Util::outputInfo(outputString);
    }
}

void GameManager::moveEnemy(Enemy *enemy, char _dir){
    switch(_dir){
        case 'r':
                enemy->getCoords().setX(enemy->unitCoords.getX()+1);
            break;
        case 'l': 
                enemy->getCoords().setX(enemy->unitCoords.getX()-1);
            break;
        case 'u':
                enemy->getCoords().setY(enemy->unitCoords.getY()-1);
            break;
        case 'd':   
                enemy->getCoords().setY(enemy->unitCoords.getY()+1);
            break;
    }
}

//this is kinda buggy :(
int GameManager::redrawEnemy(Enemy *enemy){
    if(!enemy->isEmpty()){
        Util::drawConsoleCursorPosition(enemy->getCoords(), '.');
        Util::drawConsoleCursorPosition(enemy->getPrevCoords(), '.');
        level.setTile(enemy->getPrevCoords(),'.');
        
        //enemy->getCoords().setX(enemy->getCoords().getX()-1);
        
        /* The algorithm is simple. Check where we need to go, and then look at the tile where we want to step.
         * if this tile is anything, but floor, then we do nothing.
         */
        switch(enemy->calculatePath(player.unitCoords)){ //this will return a direction
            case 'r':
                    switch(level.getTile(enemy->getCoords(), enemy->calculatePath(player.unitCoords))){
                        case '.': moveEnemy(enemy, 'r');
                            break;
                        case '@': player.takeDamage(enemy->dealDamage());
                            break;
                        default : 
                            break;
                    }
                break;
            case 'l':                       
                    switch(level.getTile(enemy->getCoords(), 'l')){
                        case '.': moveEnemy(enemy, 'l');
                            break;
                        case '@': player.takeDamage(enemy->dealDamage());
                            break;
                        default : 
                            break;
                    }
                break;
            case 'u':
                    switch(level.getTile(enemy->getCoords(), 'u')){
                       case '.': moveEnemy(enemy, 'u');
                            break;
                        case '@': player.takeDamage(enemy->dealDamage());
                            break;
                        default : 
                            break;
                    }
                break;
            case 'd':
                    switch(level.getTile(enemy->getCoords(), 'd')){
                        case '.': moveEnemy(enemy, 'd');
                            break;
                        case '@': player.takeDamage(enemy->dealDamage());
                            break;
                        default : 
                            break;
                    }
                break;
        }
        
        Util::drawConsoleCursorPosition(enemy->getCoords(), enemy->getSymbol());
        level.setTile(enemy->getCoords(), enemy->getSymbol()); 
        
        return 1;
    }
    return 0;
}

//and, actually, processing input
void GameManager::getInput(){
    switch(getch()){
                case 'M':   processInput('r');
                    break;
                case 'K':   processInput('l');
                    break;
                case 'H':   processInput('u');
                    break;
                case 'P':   processInput('d');
                    break;
                case '>':   if(level.enterCoords == player.unitCoords){
                                if(levelFullFileName == generateLevelFileNameAtIndex(levelsFolder, 1))
                                    Util::outputInfo("No one can escape that easy!", LevelCoords(5,18));
                                else{
                                    level.reload(generateLevelFileNameAtIndex(levelsFolder, levelIndex), &player);
                                    levelIndex++;
                                }
                            }          
                
                    break;
                case '<':   
                            if(level.exitCoords == player.unitCoords){
                                if(levelFullFileName == generateLevelFileNameAtIndex(levelsFolder, 5))
                                    Util::outputInfo("No one can go that deep!", LevelCoords(5,18));
                                else{
                                    level.reload(generateLevelFileNameAtIndex(levelsFolder, levelIndex), &player);
                                    levelIndex--;
                                }
                            }
                    break;
                case 27: {
                        string _choice;
                        Util::outputInfo("Are you sure you want to exit?(yes/no)", LevelCoords(5, 19));
                        Util::setConsoleCursorPosition(LevelCoords(5, 20));
                        cin>>_choice;
                        _choice = Util::toLowerCase(_choice, 'l');
                        if(_choice == "yes" || _choice == "y")
                            exit(0);
                        }
                    break;
                default: player.rest();
                    break;
            }
    }
        /*
        75 - left               M
        77 - right              K
        72 - up                 H
        80 - down               P
        
        
    
     *          ...
     *          .@.
     *          ...
     * 
     *          left = @.coords(x-1, y);
     *          right = @.coords(x+1, y);
     *          up = @.coords(x, y-1);
     *          down = @.coords(x, y+1);
*/

void GameManager::gameLoop(){
    //sorry for that
    vector<Enemy> *tempVector;
    while(player.getHp()>0){
                
        tempVector = level.getEnemies();
        getInput();
        
        for(unsigned int i = 0; i<tempVector->size(); i++){
            if(((*tempVector)[i]).isEmpty()){

                Util::eraseTile((*tempVector)[i].unitCoords);
                level.setTile((*tempVector)[i].getCoords(), '.');
                tempVector->erase(tempVector->begin()+i);
            }
            else{
                try{
                    redrawEnemy(&(*tempVector).at(i));
                }catch(...){
                    cout<<"All gone bad"<<endl;
                }
            }
        }
    }
}
/*
 * TODO - complete the rule. But that's gona be later, when read/draw is finished
 * P.S. Well, the read/draw functionality is complete, but this is gonna be done
 * later, when the basics of a game are ready, as an additional feature.
string generateLevelFileName(string _template){
    char *temp
    
    
    string ret(temp);
    return ret;
}
 * 
 * And a little piece of what was here
 *      char key = getch();
        cout<<(int)key;
        switch(key){
            case 75:player->coorX -=1;
                    cout<<player->coorX<<" "<<player->coorY<<endl;
                break;
            case 77:player->coorX +=1;
                    cout<<player->coorX<<" "<<player->coorY<<endl;
                break;
            case 72:player->coorY -=1;
                    cout<<player->coorX<<" "<<player->coorY<<endl;
                break;
            case 80:player->coorY +=1;
                    cout<<player->coorX<<" "<<player->coorY<<endl;
                break;
            default: break;
        }
 *  And a slightly modified version of that
 * switch(key){
                case 75:player->unitCoords.setX(player->unitCoords.getX()-1);
                    break;
                case 77:player->unitCoords.setX(player->unitCoords.getX()+1);
                    break;
                case 72:player->unitCoords.setX(player->unitCoords.getY()-1);
                    break;
                case 80:player->unitCoords.setX(player->unitCoords.getY()+1);
                    break;
                default: break;
            }
 * 
 * 
*/

GameManager::GameManager(string _levelsFolder)
{
    //DONE create a custom instance of a player, which involves creating !a new constructor, 
    //                                            but an init function
    player.playerInit(10,0,1);
                    //hp,ap,dmg
    levelsFolder = _levelsFolder;
    
    //ALMOST_DONE create a rule for naming levels, so they are easily accessible
    //i.e. levels/level1.lvl
    //     levels/level2.lvl
    levelIndex = 1;
    
    
    //string _template = "level%d.lvl"
    string levelFileName = "level1.lvl";
    string fullLevelFileName = levelsFolder+"\\"+levelFileName;
    
    level.load(generateLevelFileNameAtIndex(levelsFolder, levelIndex));
    level.draw();
    
    gameLoop();
}

GameManager::~GameManager()
{
}