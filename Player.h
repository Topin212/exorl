#ifndef PLAYER_H
#define PLAYER_H

#include "Unit.h" // Base class: Units
#include "Util.h"


class Player : public Unit{
public:
    LevelCoords                 unitCoords;
    LevelCoords                 prevUnitCoords;
    
    char                        unitSymbol;
    string                      unitName;
    
    void                        takeDamage(int);
    int                         dealDamage();
    
    int                         getHp();
    int                         getXp();
    void                        setXp(int xp);

    void                        die();
    void                        rest();
    void                        playerInit(int,int,int);
    
    Player();
    Player(string _name, int _hp, int _ap, int _dmg, int _xp, LevelCoords);

    ~Player();
private:
    int                         hp, ap, damage, xp, level;
    char                        playerSymbol;
    string                      name;
};

#endif // PLAYER_H
