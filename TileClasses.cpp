#include "TileClasses.h"

Enemy TileClasses::getEnemy(){
    return enemy;
}

void TileClasses::setTile(LevelCoords _tile){
    tile = _tile;
}

LevelCoords TileClasses::getTile(){
    return tile;
}
    
void TileClasses::setClasses(list<uint> _classes){
    classes = _classes;
}
list<uint> TileClasses::getClasses(){
    return classes;
}


TileClasses::TileClasses(LevelCoords _tile, Enemy _enemy){
    tile = _tile;
    enemy = _enemy;
}


TileClasses::TileClasses(LevelCoords _tile, list<uint> _classes){
    tile = _tile;
    classes = _classes;
}
TileClasses::~TileClasses(){}