#ifndef LEVEL_H
#define LEVEL_H

#include "LevelCoords.h"
#include "Player.h"
#include "Enemy.h"

class Level
{
public:
    Level();
    
    void                        reload(string, Player*);
    
    [[deprecated("set Player is not used anymore, simply draw a player on the level map")]]
    void                        setPlayer(LevelCoords, Player&);
    
    void                        setTile(LevelCoords, char);
    char                        getTile(int, int);
    char                        getTile(LevelCoords);
    char                        getTile(LevelCoords, char);
    LevelCoords                 getAdjTile(LevelCoords, char);
    LevelCoords                 searchSymbol(char symbol);
    
    LevelCoords                 enterCoords;
    LevelCoords                 exitCoords;
    
    vector<Enemy>*              getEnemies();
    Enemy&                      getEnemy(LevelCoords);
    
    void                        processLevel();
    
    void                        load(string);
    void                        draw();
    ~Level();
    
private:
    fstream                     file;
    LevelCoords                 playerCoords;
    vector<LevelCoords>         trapsCoords;
    vector<string>              levelLayout;
    vector<Enemy>               enemies;
};

#endif // LEVEL_H
