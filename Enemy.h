#ifndef ENEMIE_H
#define ENEMIE_H

#include "Unit.h" // Base class: Unit
#include "Util.h"

using std::string;

class Enemy : public Unit
{
public:
    string                      type;

    LevelCoords                 unitCoords;
    LevelCoords                 prevUnitCoords;
    
    string                      name; 
    
    string                      origin; //hell creature? heaven creature?
    string                      ability; //strong? weak? fast?
    
    std::string                 unitName;
    
    void                        setEnemy(LevelCoords);
    
    int                         isEmpty();
    
    char                        calculatePath(LevelCoords);
    
    LevelCoords&                getCoords();
    LevelCoords&                getPrevCoords();
    int&                        getHp();
    char&                       getSymbol();
    int                         getXpValue();
    
    void                        takeDamage(int);
    int                         dealDamage();
    void                        die();
    
    Enemy();
    Enemy(LevelCoords);
    Enemy(string _type, char _symbol, int _hp, int _ap, int _damage, int _xpValue, LevelCoords*);
    [[deprecated]]
    Enemy(string _type, char _symbol, int _hp, int _ap, int _damage, int _xpValue, int _posX, int _posY);
    ~Enemy();
private:
    char                        unitSymbol;
    int                         hp,
                                ap,
                                damage,
                                xpValue;
    int                         empty;
};
#endif