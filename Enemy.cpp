#include "Enemy.h"
#include "Util.h"

int Enemy::getXpValue(){
    return xpValue;
}

char Enemy::calculatePath(LevelCoords _player){
    prevUnitCoords = unitCoords;
    //which axis to move on?
    if(abs(unitCoords.getX() - _player.getX())>=abs(unitCoords.getY() - _player.getY())){
        //move on the x axis. Which way?
        if(unitCoords.getX()>=_player.getX()){
            //move left
            //actually, move right
            
            return 'l';
        }
        else{//move right
            //actually, move left
            
            return 'r';
        }
            
    }
    else{
        //move on the y 
        if(unitCoords.getY()>=_player.getY()){
            //move up
            //
            return 'u';
        }
        else{
            //move down
            //
            return 'd';
        }
    }
    prevUnitCoords = unitCoords;
    /*
     *          000000000011111111112222222222
     *          012345678901234567890123456789 
     *         0..............................
     *         1.......................@......
     *         2..............................
     *         3..g...........................
     *         4..............................
     *          
     *          
     *          g[2][3]
     *          @[23][1]
     * 
     *          
     * 
                    0         1         2
     *          012345678901234567890123456789 
     *         0..............................
     *         1..@...........................
     *         2.....................g........
     *         3..............................
     *         4..............................
     * 
     *          if((g.x()-@.x())>(g.y()-@.y())
     *              then move on the x
     *              if(g.x()>@.x()
     *                  g--
     *          else
     *              move on the y
     * 
     *          g[21][2]
     *          @[2][1]
     */
}

char& Enemy::getSymbol(){
    return unitSymbol;
}

int& Enemy::getHp(){
    return hp;
}

LevelCoords& Enemy::getCoords(){
    return unitCoords;
}

LevelCoords& Enemy::getPrevCoords(){
    return prevUnitCoords;
}

void Enemy::die(){
    empty = 1;
    string output = name + " was killed!";
    Util::outputInfoColor(output, LevelCoords(5, 24), 2);
}

void Enemy::takeDamage(int _damage){
    if(_damage>ap)
        _damage -= ap;
    else
        _damage = 0;
    
    this->hp -= _damage;
    
    if(hp<0){
        die();
    }
}

int Enemy::dealDamage(){
    return this->damage;
}

int Enemy::isEmpty(){
    return empty;
}

Enemy::Enemy(){empty=1;}

//Temp change from 
//Enemy::Enemy(string _type, char _symbol, int _hp, int _ap, int _damage, int _xpValue, LevelCoords _position)
//            : type(_type), unitSymbol(_symbol), hp(_hp), ap(_ap), damage(_damage),xpValue(_xpValue){
//to:

Enemy::Enemy(string _type, char _symbol, int _hp, int _ap, int _damage, int _xpValue, LevelCoords *_position)
            : type(_type), unitSymbol(_symbol), hp(_hp), ap(_ap), damage(_damage),xpValue(_xpValue){
    type = _type;
    
    unitCoords.set(_position);
    prevUnitCoords.set(unitCoords);
    empty = 0;
    name = Util::generateName();
}
                
Enemy::Enemy(string _type, char _symbol, int _hp, int _ap, int _damage, int _xpValue, int _posX, int _posY)
            : type(_type), unitSymbol(_symbol), hp(_hp), ap(_ap), damage(_damage),xpValue(_xpValue){
    
    unitCoords.set(_posX, _posY);
    prevUnitCoords.set(unitCoords);
    empty = 0;
    name = Util::generateName();
}

Enemy::Enemy(LevelCoords position){
    empty=0;
    hp = 3;
    ap = 0;
    damage = 1;
    unitCoords = position;
    prevUnitCoords = unitCoords;
}

/*void Enemy::setEnemy(LevelCoords position){
    prevUnitCoords = unitCoords = position;
}*/

Enemy::~Enemy(){}