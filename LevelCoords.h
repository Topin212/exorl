#ifndef LEVELCOORDS_H
#define LEVELCOORDS_H

class LevelCoords{
public:
    LevelCoords();
    LevelCoords(int,int);
    
    int operator==(LevelCoords);
    operator bool() const;
    
    LevelCoords&                invertCoords();
    
    int                         getX();
    int                         getY();

    void                        setX(int);
    void                        setY(int);
    //should this return value? like 1, for being a success?
    void                        set(int, int);
    void                        set(LevelCoords);
    void                        set(LevelCoords*);
    ~LevelCoords(){};
private:
    int x, y;
};


#endif // LEVELCOORDS_H
