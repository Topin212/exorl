#include "Menu.h"

void Menu::displayGameName(string _gameName){
    Util::outputInfo(_gameName, LevelCoords(40 - _gameName.length()/2, 1));
}

void Menu::displayCentered(string _what){
    Util::outputInfoColor(_what, LevelCoords(40 - _what.length()/2, height),8);
    height++;
    menuList.emplace_back(MenuObject(_what, height, 0));
}

void Menu::displayCenteredActive(string _what){
    Util::outputInfoColor(_what, LevelCoords(40 - _what.length()/2, height),7);
    height++;
    menuList.emplace_back(MenuObject(_what, height, 1));
}

void Menu::displayCenteredInActive(string _what){
    Util::outputInfoColor(_what, LevelCoords(40 - _what.length()/2, height),8);
    height++;
}

void Menu::displayPlayGame(){
    string _str("Play Game");
    Util::outputInfo(_str, LevelCoords(40 - _str.length()/2, height));
    height++;
}

void Menu::displayExit(){
    string _str("Exit");
    Util::outputInfo(_str, LevelCoords(40 - _str.length()/2, height));
    height++;
}

Menu::MenuObject::MenuObject(string _name, int _height, int _active): name(_name), height(_height), active(_active){}


Menu::Menu(): height(5){}
Menu::~Menu(){}