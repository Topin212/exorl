#ifndef MENU_H
#define MENU_H

#include "Util.h"

class Menu
{
public:
    void                        displayGameName(string);
    void                        displayCentered(string);
    void                        displayCenteredActive(string);
    void                        displayCenteredInActive(string);

    void                        displayPlayGame();
    void                        displayExit();
    
    class MenuObject{
    public:
    //once again, this class knows what to display, at thich heights and whether it's active or not
        string name;
        int height;
        int active;
        MenuObject(string, int, int);
        ~MenuObject(){};
    };
    
    list<MenuObject> menuList;
    
    Menu();
    ~Menu();
private:
    int height;
};

#endif // MENU_H
