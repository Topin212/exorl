#include "Util.h"

LevelCoords Util::output = LevelCoords(5,20); //y,x
LevelCoords Util::outputEnemy = LevelCoords(5, 21); //y, x?
LevelCoords Util::outputExtra = LevelCoords(5, 22); //y, x?
HANDLE Util::consoleHandle = GetStdHandle(STD_OUTPUT_HANDLE);
//7 = gray
//8 = darkgray

void Util::outputInfoColor(string _message, LevelCoords _where, int _color){
    SetConsoleTextAttribute(consoleHandle, _color);
    setConsoleCursorPosition(_where);
    cout<<_message;
    SetConsoleTextAttribute(consoleHandle, 7);
}


void Util::outputInfoColor(string _message, int _color){
    SetConsoleTextAttribute(consoleHandle, _color);
    setConsoleCursorPosition(output);
    cout<<_message;
    SetConsoleTextAttribute(consoleHandle, 7);
}

char Util::getInput(){
    switch(getch()){
        case 'M':   return 'r';
            break;
        case 'K':   return 'l';
            break;
        case 'H':   return 'u';
            break;
        case 'P':   return 'd';
            break;
        case 27:    return 27;
            break;
        default:    break;
    }
    return '\0';
}

string Util::toLowerCase(string _str, char upperlower){
    if(upperlower == 'u')
        transform(_str.begin(), _str.end(), _str.begin(), ::toupper);
    else
        transform(_str.begin(), _str.end(), _str.begin(), ::tolower);
    return _str;
}

void Util::eraseTile(LevelCoords _where){
    setConsoleCursorPosition(_where);
    cout<<'.';
}

string Util::generateName(){
    string returnStr;
    array<string, 6> prefix = {"Vil", "Ram", "Dum", "Bil", "Sak", "Ten"};
    array<string, 6> root = {"Inbo", "Zak", "Zumba", "Della", "Trimba", "Bailo"};
    array<string, 6> job = {"axemaster", "swordMaster", "crossbowman", "bowman", "thrower", "berserk"};
    array<string, 6> origin = {"Heavens", "Magical", "Weak", "Heretic", "Heroic", "Monstrous"};
    
    auto generateRandom  = Util::generateRandomNumber;
    
    returnStr = prefix[generateRandom(0, prefix.size())]+" "+root[generateRandom(0, prefix.size())]+" the "+origin[generateRandom(0, prefix.size())]+" "+job[generateRandom(0, prefix.size())];
    return returnStr;
}

void Util::outputInfo(string message, LevelCoords _where){
    if(_where){
        setConsoleCursorPosition(_where);
        cout<<message<<endl;
    }
    else{
        setConsoleCursorPosition(output);
        cout<<message<<endl;
    }
}

void Util::outputInfo(LevelCoords _coords, LevelCoords _where){
    if(_where){
        setConsoleCursorPosition(_where);
        cout<<"X: "<<_coords.getX()<<"Y: "<<_coords.getY()<<endl;
    }
    else{
        setConsoleCursorPosition(output);
        cout<<"X: "<<_coords.getX()<<"Y: "<<_coords.getY()<<endl;
    }
}

void Util::outputInfo(int message, LevelCoords _where){
    if(_where){
        setConsoleCursorPosition(_where);
        cout<<message<<endl;
    }
    else{
        setConsoleCursorPosition(output);
        cout<<message<<endl;
    }
}

int Util::generateRandomNumber(int min, int max){
    int tempval;
    srand(time(NULL));
    tempval=rand()%max + min;
    return tempval;
}

void Util::drawConsoleCursorPosition(LevelCoords coords, char tile){
    setConsoleCursorPosition(coords);
    cout<<tile;
}


//THIS ONE IS WORKING! WHERE'S MA BEER, BABY?
void Util::setConsoleCursorPosition(LevelCoords coords){
    HANDLE consoleWindowHandle;
    consoleWindowHandle = GetStdHandle(STD_OUTPUT_HANDLE);
    
    COORD cursorPosition;
    cursorPosition.X=coords.getX();
    cursorPosition.Y=coords.getY();

    
    if(!SetConsoleCursorPosition(consoleWindowHandle, cursorPosition)){
        system("cls");
        cout<<"Setting cursor gone bad. Try saving your level and restarting the game. Believe me, this is your only option.";
    }
    
}