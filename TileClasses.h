#ifndef TILECLASSES_H
#define TILECLASSES_H

#include "LevelCoords.h"
#include "Util.h"
#include "Enemy.h"

[[deprecated]]
class TileClasses
{
public:
    void                setTile(LevelCoords);
    LevelCoords         getTile();
    
    void                setClasses(list<uint>);
    [[deprecated("wasn't even done :(")]]
    list<uint>          getClasses();
    Enemy               getEnemy();
    
    
    
    TileClasses(LevelCoords, list<uint>);
    TileClasses(LevelCoords, Enemy);
    ~TileClasses();
private:
    LevelCoords tile;
    list<uint> classes;
    Enemy enemy;
};

#endif // TILECLASSES_H
