#ifndef UNIT_H
#define UNIT_H

#include <string>
#include "LevelCoords.h"

class Unit
{
public:
/*    LevelCoords unitCoords;
    LevelCoords prevUnitCoords;
    
    std::string unitName;
    char unitSymbol;*/
    
    Unit();
    
    //this will be used for traps, but not combats;
    void                        takeDamage(int damage);
    //this will be used for combats, but not traps;
    int                         dealDamage();
    
    ~Unit();
};

#endif // UNIT_H