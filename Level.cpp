#include "Level.h"

void Level::reload(string level, Player *player){
    system("cls");
    //for(int i = 0; i<80; i++)
    //    for(int j = 0; j<30; j++)
    //        Util::drawConsoleCursorPosition(LevelCoords(i,j), ' ');
    for(int i = enemies.size(); i-- ;){
        enemies.pop_back();
    }
    for(int i = levelLayout.size(); i--;){
        levelLayout.pop_back();
    }
    
    load(level);
    player->unitCoords = enterCoords;
    draw();
}

char Level::getTile(LevelCoords _where){
    return levelLayout[_where.getY()][_where.getX()];
}

Enemy& Level::getEnemy(LevelCoords _coords){
    unsigned int i;
    for(i = 0; i<enemies.size(); i++){
        if(enemies[i].unitCoords == _coords){
            return enemies[i];
        }
    }
    throw i;
}

vector<Enemy>* Level::getEnemies(){
    return &enemies;
}

//reading file graphics.
void Level::load(string fileName){
    file.open(fileName.c_str(), ios::in);
    
    if(!file.is_open()){
        cout<<"File "<<fileName<<" is not open\n";
        //throw LevelFileNotFoundException; 
        system("pause");
    }
    
    string buffer;
    LevelCoords tempCoords;
    
    for(unsigned int i = 0; getline(file, buffer); i++){
        levelLayout.push_back(buffer);
        for(unsigned int j = 0;j<levelLayout[i].size(); j++){
            tempCoords.set(j,i);
            //this level processing will let users to create their custom levels
            switch(levelLayout[i][j]){
                case '@':
                //Player needs to be constructed here, 
                break;
                case 'g':{  //goblin
                        LevelCoords tempCoords(j,i);
                //                          name        symbol          hp ap dmg xp   position
                }enemies.emplace_back(Enemy("goblin", levelLayout[i][j], 10, 5, 2, 2, &tempCoords));
                break;
                case 'O':{ //ogre
                        LevelCoords tempCoords(j,i);
                //                          name        symbol         hp ap dmg xp   position
                enemies.emplace_back(Enemy("ogre", levelLayout[i][j], 50, 10, 5, 15, &tempCoords));
                }break;
                case 'D':{//dragon
                        LevelCoords tempCoords(j,i);
                //                          name        symbol         hp ap dmg xp   position
                enemies.emplace_back(Enemy("dragon", levelLayout[i][j], 50, 10, 5, 15, &tempCoords));
                }break;
                case 'G':{//goblin warlord
                        LevelCoords tempCoords(j,i);
                //                          name        symbol         hp ap dmg xp   position
                enemies.emplace_back(Enemy("goblin warlord", levelLayout[i][j], 50, 10, 5, 15, &tempCoords));
                }break;
                case 'B':{//bandit
                        LevelCoords tempCoords(j,i);
                //                          name        symbol         hp ap dmg xp   position
                enemies.emplace_back(Enemy("bandit", levelLayout[i][j], 50, 10, 5, 15, &tempCoords));
                }break;
                case '>':{//bandit
                        enterCoords = LevelCoords(j,i);
                        }break;
                case '<':{//bandit
                        exitCoords = LevelCoords(j,i);
                        }break;
            }
        }
        
    }
    file.close();
}

//print level graphics.
void Level::draw(){
    LevelCoords coords;
    for(unsigned int i = 0; i<levelLayout.size(); i++){ //width
        for(unsigned int j = 0; j<levelLayout[i].size(); j++){//height
            coords.set(j,i);
            Util::drawConsoleCursorPosition(coords, levelLayout[i][j]);
        }
    }
    
    
    /*Depricated function
     * for(int i = 0; levelLayout.size(); i++){
        cout<<levelLayout[i]<<endl;
    }
    cout<<"Level drawn\n";*/
}

char Level::getTile(int x, int y){
    return levelLayout[x][y];
}

char Level::getTile(LevelCoords lc, char dir){
    switch(dir){
        case 'u': return levelLayout[lc.getY()-1][lc.getX()]; break;
        case 'd': return levelLayout[lc.getY()+1][lc.getX()]; break;
        case 'l': return levelLayout[lc.getY()][lc.getX()-1]; break;
        case 'r': return levelLayout[lc.getY()][lc.getX()+1]; break;
        default:return 0;break;
    }
}

void Level::setTile(LevelCoords _where, char _newTile){
    levelLayout[_where.getY()][_where.getX()] = _newTile;
}


LevelCoords Level::getAdjTile(LevelCoords lc, char dir){
    switch(dir){
        case 'u': return LevelCoords(lc.getY()-1,lc.getX()); break;
        case 'd': return LevelCoords(lc.getY()+1,lc.getX()); break;
        case 'l': return LevelCoords(lc.getY(),lc.getX()+1); break;
        case 'r': return LevelCoords(lc.getY(),lc.getX()-1); break;
        default:return LevelCoords();break;
    }
}

void Level::setPlayer(LevelCoords where, Player& player){
    //levelLayout[player.coorX][player.coorY] = '@';
}



//LevelCoords block
//============================


LevelCoords Level::searchSymbol(char symbol){
    LevelCoords returnCoords;
    for(unsigned int i = 0; i<levelLayout.size(); i++)//height
        for(unsigned int j = 0; j<levelLayout[i].size(); i++){//width
            if(levelLayout[i][j] == symbol){
                returnCoords.set(j,i);
                return returnCoords;
            }
        }
        return LevelCoords(-1, -1);
}

Level::Level(){}
Level::~Level(){}